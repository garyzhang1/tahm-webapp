Tahm --- an O2O catering application with MEAN architecture, and codes are deposited on Bitbucket with services  deployed on Bluemix.
This application allows you to display, order and pay, with the results updating in real-time. 

The app uses the following libraries and frameworks:
Node.js
Express
Jade templates
Mongoose
AngularJS
Bootstrap 3
Socket.io

Demo
You can see the app running live at http://******.bluemix.net/.

License
The source code for the app is available under the MIT license, which is found in license.txt in the root of this repository.
